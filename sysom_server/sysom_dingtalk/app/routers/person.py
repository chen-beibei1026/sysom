# -*- coding: utf-8 -*- #
"""
Time                2023/12/04 17:53
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                health.py
Description:
"""
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.query import PersonQueryParams
from app.database import get_db
from app.crud import get_person_list, get_person_by_name
from app.schemas import Person
from sysom_utils import StandardListResponse, StandardResponse


router = APIRouter()


@router.get("/get")
async def get_specific_person(
    person_name: str, db: Session = Depends(get_db)
):
    person = get_person_by_name(db, person_name)
    return StandardResponse(person, Person)

@router.get("/list")
async def get_persons(
    query_params: PersonQueryParams = Depends(), db: Session = Depends(get_db)
):
    person_list = get_person_list(db, query_params)
    return StandardListResponse(person_list, Person)
