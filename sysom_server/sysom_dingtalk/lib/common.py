# -*- coding: utf-8 -*-
import time
import hmac
import base64
import os,sys,json
import traceback
import urllib
import hashlib
import datetime
import requests

def get_group_postlink(webhook,sec):
    try:
        link = webhook
        timestamp = str(round(time.time() )) + '000'
        # for python2: timestamp = str(int(time.time() * 1000))
        secret = sec
        secret_enc = secret.encode('utf-8')
        string_to_sign = '{}\n{}'.format(timestamp, secret)
        string_to_sign_enc = string_to_sign.encode('utf-8')
        hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
        sign = urllib.parse.quote_plus(base64.b64encode(hmac_code))
        # for python2: sign = urllib.quote_plus(base64.b64encode(hmac_code))
        link = "%s&timestamp=%s&sign=%s"%(link,timestamp,sign)
        print (link)
        return link
    except:
        traceback.print_exc()
        print ('get_group_postlink exception!')
        pass
        return ""

def postback(webhook_link, ret_msg):
    try:
        header= {"Content-Type": "application/json"}
        res = requests.post(webhook_link, data=json.dumps(ret_msg),headers=header)
        print (res.json())
        if "system busy" in res:
            time.sleep(5)
            print ("wait 5 senconds!")
            res = requests.post(url, data=json.dumps(data),headers=header)
            print (res.json())
    except:
        traceback.print_exc()

def timestamp_to_formattime(ts):
    try:
        k = len(str(ts)) - 10
        time_t = datetime.datetime.fromtimestamp(ts/(1* 10**k))
        datetimeValue = time_t.strftime("%Y-%m-%d %H:%M:%S")
        return datetimeValue
    except Exception as e:
        logger.exception(e)
        return ts
