# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                schemas.py
Description:
"""
import time
import enum
from pydantic import BaseModel, validator
from datetime import datetime
from typing import List, Optional
from clogger import logger

###########################################################################
# Define schemas here
###########################################################################

# @reference https://fastapi.tiangolo.com/zh/tutorial/response-model/
# class Person(BaseModel):
#     id: int
#     name: str
#     age: int
    
#     class Config:
#         orm_mode = True

class NodeLogBase(BaseModel):
    ts: str 
    instance: str
    event_type: str
    description: str
    extra: dict
    
class NodeLogCreate(NodeLogBase):
    pass


class NodeLog(NodeLogBase):
    id: int
    create_at: datetime
    update_at: datetime
    
    class Config:
        orm_mode = True
        
        
class AuditLogBase(BaseModel):
    methond: str
    ip: str
    path: str
    browser_agent: str
    handler: str
    request_type: str
    status: int
    user: Optional[int] = 0
    
class AuditLogCreate(AuditLogBase):
    pass

class AuditLog(AuditLogBase):
    id: int
    create_at: datetime
    update_at: datetime
    
    class Config:
        orm_mode = True


# class NodeFieldModel(BaseModel):
#     ts: str 
#     instance: str
#     event_type: EventTypeEnum
#     description: str
#     extra: dict

#     class Config:
#         orm_mode = True


# class ResponseModel(BaseModel):
#     code: int = 200
#     message: str = "success"
#     data: Optional[List]


# class AuditOutpuModel(BaseModel):
#     id: int
#     methond: str
#     ip: str
#     path: str
#     browser_agent: str
#     handler: str
#     request_type: str
#     create_at: datetime
#     status: int

#     class Config:
#         orm_mode = True


# class NodeLogOutput(BaseModel):
#     id: int
#     ts: int
#     instance: str
#     event_type: str
#     description: str
#     extra: str

#     class Config:
#         orm_mode = True

#     @validator("ts")
#     def check_ts(cls, ts):
#         return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(ts))


# class AuditResponseModel(ResponseModel):
#     data: List[AuditOutpuModel]
#     total: int


# class NodeResponseModel(ResponseModel):
#     total: int
#     data: List[NodeLogOutput]