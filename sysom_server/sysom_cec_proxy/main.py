# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                ssh.py
Description:
"""
from clogger import logger
from fastapi import FastAPI
from conf.settings import YAML_CONFIG
from app.routers import health, proxy
from sysom_utils import CmgPlugin, SysomFramework


app = FastAPI()

app.include_router(health.router, prefix="/api/v1/cec_proxy/health")
app.include_router(proxy.router, prefix="/api/v1/cec_proxy/proxy")

#############################################################################
# Write your API interface here, or add to app/routes
#############################################################################


def init_framwork():
    SysomFramework\
        .init(YAML_CONFIG) \
        .load_plugin_cls(CmgPlugin) \
        .start()
    logger.info("SysomFramework init finished!")


@app.on_event("startup")
async def on_start():
    init_framwork()
    
    #############################################################################
    # Perform some microservice initialization operations over here
    #############################################################################


@app.on_event("shutdown")
async def on_shutdown():
    pass