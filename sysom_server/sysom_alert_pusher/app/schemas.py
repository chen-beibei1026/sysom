# -*- coding: utf-8 -*- #
"""
Time                2023/11/23 19:11
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                schemas.py
Description:
"""
from pydantic import BaseModel
from enum import Enum

###########################################################################
# Define schemas here
###########################################################################

from typing import List

class AlertLevel(str, Enum):
    WARNING = "WARNING"
    ERROR = "ERROR"
    CRITICAL = "CRITICAL"

class AlertType(str, Enum):
    MONITOR = "MONITOR"  # 监控告警
    APPLICATION = "APPLICATION"  # 应用告警
    OTHER = "OTHER"  # 其它类型告警


class AlertStatus(str, Enum):
    NORMAL = "NORMAL"
    PENDING = "PENDING"
    FIRING = "FIRING"
    RESOLVED = "RESOLVED"


class AlertData(BaseModel):
    """SysOM Alert data format definition

    Attributes:
        alert_id(str): 告警ID（使用 uuid v4 生成）
        instance(str): 告警实例
        alert_item(str): 告警项，用于唯一标识一类告警，比如每个告警规则可以对应一个告警项
        alert_category(AlertType): 告警类别
        alert_source_type(str): 告警源类型，例如：Grafana、Alert
        alert_time(int): 告警发生时间，采用时间戳，单位为 ms
        status(AlertStatus): 告警状态 normal -> pending -> firing -> resolved
        labels(dict): 告警标签
        annotations(dict): 告警注释
        origin_alert_data: 原始告警数据
    """

    alert_id: str
    instance: str
    alert_item: str
    alert_category: AlertType
    alert_source_type: str
    alert_level: AlertLevel = AlertLevel.WARNING
    alert_time: int
    status: AlertStatus = AlertStatus.FIRING
    labels: dict = {}
    annotations: dict = {}
    origin_alert_data: dict = {}