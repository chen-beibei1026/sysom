import re
from clogger import logger
from sysom_utils.adddict import Dict
from .base import PushRuleBase


class PushRuleRegex(PushRuleBase):
    def __init__(self, config: dict) -> None:
        super().__init__(config)
        self.rules = config.get("rules", {})
    
    def is_match(self, data: dict) -> bool:
        data = Dict(data)
        for field, parttern in self.rules.items():
            value = data.get_multi(field)
            if value is None:
                logger.warning(f"PushRuleRegex: Field not found, field = {field}")
                return False
            if re.match(parttern, value) is None:
                logger.warning(f"PushRuleRegex:  Regex not match, field = {field}, parttern = {parttern}")
                return False
        return True