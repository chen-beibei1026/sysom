'''
Channel Service Gunicorn Settings
'''
from conf.settings import YAML_CONFIG

bind = YAML_CONFIG.get_service_config().get("bind", "127.0.0.1")
port = YAML_CONFIG.get_service_config().get("port", "80")

workers = 2  # 指定工作进程数

threads = 3

bind = f'{bind}:{port}'

worker_class = 'uvicorn.workers.UvicornWorker'  # 工作模式线程, 默认为sync模式

max_requests = 2000      # 设置最大并发数量为2000 (每个worker处理请求的工作线程)

accesslog = '/var/log/sysom/sysom-channel-access.log'

loglevel = 'error'

proc_name = 'channel_service'
