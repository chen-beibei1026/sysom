from lib.metric_type.metric_type import Metric, MetricReader,\
    Level, RangeAggregationType, InsAggregationType
from lib.metric_exception import MetricSettingsException


class ErrorMetric(Metric):
    def __init__(self, metric_reader: MetricReader, metric_settings,
                 level: Level):
        super().__init__(metric_reader, metric_settings, level)

    def _collect_process_metric(self) -> float:
        if self.settings.collect.standard_type != 1:
            raise MetricSettingsException()

        return super()._default_single_counter(
            related_value=self.settings.collect.related_value[0],
            range_agg_type=RangeAggregationType.Increase,
            ins_agg_type=InsAggregationType.Sum
        )

    def metric_score(self, pod: str, node: str,
                     cluster: str, last_end_time: float) -> (float, float):
        return super().metric_score(pod, node, cluster, last_end_time)
