from lib.metric_type.metric_type import Metric, MetricReader,\
    Level, RangeAggregationType, InsAggregationType
from lib.metric_exception import MetricSettingsException


class LoadMetric(Metric):
    def __init__(self, metric_reader: MetricReader, metric_settings,
                 level: Level):
        super().__init__(metric_reader, metric_settings, level)

    def _collect_process_metric(self) -> float:
        """
        We offer two standard method to process load metric:
        standard_type = 1: the metric is already load
        standard_type = 2: the metric is counter
        """

        standard_type = self.settings.collect.standard_type

        if standard_type == 1:
            return super()._default_single_gauge()

        elif standard_type == 2:
            return super()._default_single_counter(
                related_value=self.settings.collect.related_value[0],
                range_agg_type=RangeAggregationType.Rate,
                ins_agg_type=InsAggregationType.Max
            )
        else:
            raise MetricSettingsException(
                f'illegal standard type:{standard_type}'
            )

    def metric_score(self, pod: str, node: str,
                     cluster: str, last_end_time: float) -> (float, float):
        return super().metric_score(pod, node, cluster, last_end_time)
