# -*- coding: utf-8 -*- #
"""
Time                2023/08/24 15:41
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                ssh.py
Description:
"""
from clogger import logger
from fastapi import FastAPI
from app.routers import health, alert
from conf.settings import YAML_CONFIG
from sysom_utils import CmgPlugin, SysomFramework


app = FastAPI()

app.include_router(health.router, prefix="/api/v1/alarm/health")
app.include_router(alert.router, prefix="/api/v1/alarm")


#############################################################################
# Write your API interface here, or add to app/routes
#############################################################################


def init_framwork():
    SysomFramework\
        .init(YAML_CONFIG) \
        .load_plugin_cls(CmgPlugin) \
        .start()
    logger.info("SysomFramework init finished!")


@app.on_event("startup")
async def on_start():
    init_framwork()
    
    #############################################################################
    # Perform some microservice initialization operations over here
    #############################################################################
    
    # Start Alarm listener
    from app.executor import AlarmListener
    try:
        AlarmListener().start()
    except Exception as e:
        logger.exception(e)


@app.on_event("shutdown")
async def on_shutdown():
    pass