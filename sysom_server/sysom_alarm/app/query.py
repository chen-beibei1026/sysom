# -*- coding: utf-8 -*- #
"""
Time                2023/09/19 15:41
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                schemas.py
Description:
"""
from typing import Optional
from app import models
from sysom_utils import BaseQueryParams


class AlertDataQueryParams(BaseQueryParams):
    # 1. 指定要查询的模型
    __modelclass__ = models.AlertData

    # 2. 定义排序字段
    sort__: str = "-alert_time"

    # 3. 定义支持用于过滤的参数
    alert_id: Optional[str] = None
    instance: Optional[str] = None
    alert_item: Optional[str] = None
    alert_category: Optional[str] = None
    alert_source_type: Optional[str] = None
    status: Optional[str] = None
    deal_status: Optional[str] = None
