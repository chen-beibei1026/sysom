import sys
from clogger import logger
from django.apps import AppConfig


class MigrationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.migration'

    def ready(self):
        if ('runserver' in sys.argv or 'manage.py' not in sys.argv):
            from django.conf import settings
            from sysom_utils import CmgPlugin, SysomFramework

            SysomFramework.init(settings.YAML_CONFIG) \
                .load_plugin_cls(CmgPlugin) \
                .enable_channel_job() \
                .start()

        logger.info(">>> Migration module loading success")
