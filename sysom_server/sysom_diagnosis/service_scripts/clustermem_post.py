from typing import List
from clogger import logger
from .base import DiagnosisJobResult, DiagnosisPostProcessor, PostProcessResult
import json

class PostProcessor(DiagnosisPostProcessor):
    def parse_diagnosis_result(self, results: List[DiagnosisJobResult]) -> PostProcessResult:
        ret_str = results[0].stdout
        ret_code = 0
        ret_errmsg = ""
        ret = json.loads(ret_str)

        try:
            if ret["success"] != True:
                ret_code = 1
                ret_errmsg = ret["errmsg"]
        except:
            ret_code = 2
            ret_errmsg = "clustermem error!"
            pass

        postprocess_result = PostProcessResult(
            code=ret_code,
            err_msg=ret_errmsg,
            result={}
        )

        # 输出诊断结果
        md_msg = ""
        md_msg = "## 内存分析结果\n\n\n\n%s\n\n\n\n"%ret["root_cause"]
        md_msg = "%s## 修复/进一步排查建议\n\n\n\n%s\n\n\n\n"%(md_msg, ret["suggestion"])
        # markdown 输出诊断结果
        postprocess_result.result["ClustermemResult"] = {"data": md_msg}

        # 计算内存占比数据
        stat_data = []
        node_mem_data = ret["node_data"]["sysom_proc_meminfo"]
        node_mem_used = node_mem_data["MemTotal"] - node_mem_data["MemFree"]
        stat_data.append({"key": "节点内存使用(usage/total)",
                            "value": round(node_mem_used * 100 / node_mem_data["MemTotal"], 1)})
        stat_data.append({"key": "节点应用内存(app/usage)", 
                            "value": round(node_mem_data["user_anon"] * 100 / node_mem_used, 1)})
        stat_data.append({"key": "节点文件缓存\n(cache/usage)",
                            "value": round(node_mem_data["user_filecache"] * 100 / node_mem_used, 1)})
        stat_data.append({"key": "节点内核内存(kernel/total)", 
                            "value":round(node_mem_data["kernel_used"] * 100 / node_mem_data["MemTotal"], 1)})
        if len(ret["pod_name"]) > 0:
            pod_mem_data = ret["pod_data"]["sysom_cg_memUtil"]
            pod_anon_ratio = (pod_mem_data["usage"] - pod_mem_data["active_file"]
                                        - pod_mem_data["inactive_file"]) * 100 / pod_mem_data["usage"]
            # active_file + inactive_file可能大于usage
            pod_anon_ratio = pod_anon_ratio if pod_anon_ratio > 0 else 0.1
            stat_data.append({"key": "Pod内存使用(usage/limit)", "value": pod_mem_data["mem_util"]})
            stat_data.append({"key": "Pod应用内存(app/usage)", "value": round(pod_anon_ratio, 1)})
            stat_data.append({"key": "Pod文件缓存\n(cache/usage)", "value": pod_mem_data["cache_ratio"]})
        # stat输出内存占比
        postprocess_result.result["UsageResult"] = {"data": stat_data}
        
        # 显示podmem结果
        datas = []
        # pod page cache高，显示对应pod的podmem结果
        if ret["show_cache"] == "pod":
            pod_name = ret["pod_name"]
            pod_mem = ret["podmem_data"][pod_name]
            for file in pod_mem:
                datas.append({
                    "Pod": pod_name,
                    "文件名": file[0],
                    "文件Cached大小": file[1]
                })
        # 节点page cache高，显示node上所有pod的podmem结果
        elif ret["show_cache"] == "node":
            podmem_data = ret["podmem_data"]
            for key, value in podmem_data.items():
                for file in value:
                    datas.append({
                        "Pod": key,
                        "文件名": file[0],
                        "文件Cached大小": file[1]
                    })
        # table显示podmem结果
        postprocess_result.result["podmem"] = {"data": datas}

        return postprocess_result