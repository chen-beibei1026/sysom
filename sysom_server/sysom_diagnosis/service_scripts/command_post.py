"""
Time                2023/06/19 17:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                command.py
Description:
"""
from typing import List
from .base import DiagnosisJobResult, DiagnosisPostProcessor, PostProcessResult


class PostProcessor(DiagnosisPostProcessor):
    def parse_diagnosis_result(self, results: List[DiagnosisJobResult]) -> PostProcessResult:
        postprocess_result = PostProcessResult(
            code=0,
            err_msg="",
            result={}
        )
        postprocess_result.result = {
            "CommandResult": {"data": [{"key": "", "value": results[0].stdout}]}
        }
        return postprocess_result
