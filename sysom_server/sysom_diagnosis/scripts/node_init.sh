#!/bin/bash -x
RESOURCE_DIR=${NODE_HOME}/${SERVICE_NAME}

if [ "$SYSAK_VERTION" == "" ]; then
    export SYSAK_VERTION=2.4.0-1
fi
if [ "$ARCH" == "" ]; then
    export ARCH=x86_64
fi

function version_gt() { test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1"; }

main() {
    rpm -q --quiet dnf || yum install -y dnf
    rpm -qa | grep sysak
    if [ $? -eq 0 ]; then
        current_version=$(rpm -q sysak --queryformat "%{VERSION}-%{RELEASE}")
        if version_gt $SYSAK_VERTION $current_version; then
            echo "sysak version is too low, current upgrade to $SYSAK_VERTION"
        else
            echo "sysak version is $current_version, no need to upgrade"
            exit 0
        fi
    fi

    SYSAK_PKG=$(ls -r sysak-*.rpm)
    yum install -y ${SYSAK_PKG}

    ###install sysak from local yum repo first###
    if [ $? -ne 0 ]; then
        yum install -y sysak
    fi
}

main
