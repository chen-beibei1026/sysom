import sys
from clogger import logger
from django.apps import AppConfig
from sysom_utils import NodeDispatcherPlugin, CmgPlugin, SysomFramework


class TaskConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.task'

    def ready(self):
        from django.conf import settings
        if ('runserver' in sys.argv or 'manage.py' not in sys.argv):
            from apps.task.executor import DiagnosisTaskExecutor

            SysomFramework.init(settings.YAML_CONFIG) \
                .load_plugin_cls(NodeDispatcherPlugin) \
                .load_plugin_cls(CmgPlugin) \
                .enable_channel_job() \
                .start()

            # 这边微服务正式启动的时候执行一些处理代码
            # 启动任务结果处理线程
            try:
                DiagnosisTaskExecutor(settings.YAML_CONFIG).start()
            except Exception as e:
                logger.exception(e)
        else:
            # 这边执行数据库迁移等操作的时候执行一些处理代码
            pass
        logger.info(">>> Diagnosis module loading success")
