# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                models.py
Description:
"""
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.sql import func
from app.database import Base


###########################################################################
# Define databse model here
###########################################################################

# @reference https://fastapi.tiangolo.com/zh/tutorial/sql-databases/
# class Person(Base):
#     __tablename__ = "sys_person"
#     id = Column(Integer, primary_key=True)
#     name = Column(String(254), unique=True)
#     age = Column(Integer)
#     created_at = Column(DateTime(timezone=True), server_default=func.now())