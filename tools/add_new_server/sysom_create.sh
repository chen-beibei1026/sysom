#!/bin/bash -x
set -x
ProgName=$(basename $0)
BaseDir=$(dirname $(readlink -f "$0"))

####################################################################################################################
# Helper functions
####################################################################################################################

red() {
    printf '\33[1;31m%b\n\33[0m' "$1"
}

green() {
    printf '\33[1;32m%b\n\33[0m' "$1"
}

check_port_conflict() {
    port=$1

    # scan all service 
    local_microservice_dir=`dirname $BaseDir`/sysom_server
    for service in $(ls ${local_microservice_dir})
    do
        service_config_dir=${local_microservice_dir}/${service}/config.yml
        if [ ! -f "${service_config_dir}" ]; then
            continue
        fi
        _service_port=`cat $service_config_dir | grep 'sysom_service:' -A 6 | awk '/port/ {print $2}'`
        if [ "${port}" == "${_service_port}" ]; then
            red "Error: ${port} already used by ${service}"
            exit 1
        fi
    done
}

check_service_name_conflict() {
    service_name=$1
    local_microservice_dir=`dirname $BaseDir`/sysom_server
    for service in $(ls ${local_microservice_dir})
    do
        if [ "${service_name}" == "${service}" ];then
            red "Error: ${service} already exists"
            exit 1
        fi
    done
}

do_fill_template() {
    service_name_shorter=$3
    service_name=sysom_$3
    service_middle=sysom-$3

    for file in `ls $1`
    do
        if [ -d $1"/"$file ]
        then
            do_fill_template $1"/"$file $2 $3
        else
            target_file=$1"/"$file
            sed "s/\$TEMPLATE_SERVICE_NAME_SHORTER/$service_name_shorter/g" -i $target_file
            sed "s/\$TEMPLATE_SERVICE_NAME_MIDDLE/$service_middle/g" -i $target_file
            sed "s/\$TEMPLATE_SERVICE_NAME/$service_name/g" -i $target_file
            sed "s/\$TEMPLATE_SERVICE_PORT/$2/g" -i $target_file
        fi
    done
}


####################################################################################################################
# Subcommands
####################################################################################################################

sub_help(){
    echo "Usage: $ProgName <subcommand> [options]"
    echo "Subcommands:"
    echo "    server     Create one new server(microservice)"
    echo "        Example: $ProgName server demo"
    echo ""
    echo "For help with each subcommand run:"
    echo "$ProgName <subcommand> -h|--help"
    echo ""
}

sub_server() {
    sub_microservice $@
}

sub_service() {
    sub_microservice $@
}

sub_microservice() {
    name=$1
    service_port=$2
    service_name_shorter=$name
    service_name=sysom_$name
    service_middle=sysom-$name

    green $service_port
    
    # Check port conflict
    check_port_conflict $service_port
    
    # Check service_name conflict
    check_service_name_conflict $service_name

    # Copy template file to sysom_service and script/server
    target_script_dir=`dirname $BaseDir`/script/server/${service_name}
    target_service_dir=`dirname $BaseDir`/sysom_server/${service_name}
    pushd `dirname $BaseDir`/template/fastapi
        cp -r scripts $target_script_dir
        cp -r service $target_service_dir
    popd

    # Fill service and port info to template
    do_fill_template $target_script_dir $service_port $name
    do_fill_template $target_service_dir $service_port $name

    # rename supervisor conf
    pushd $target_script_dir
        mv template.ini $service_middle.ini
    popd
}


subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift
        sub_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac