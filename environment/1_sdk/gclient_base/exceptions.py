# -*- coding: utf-8 -*- #
"""
Time                2023/4/28 14:55
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                exceptions.py
Description:
"""


class GClientException(Exception):
    """GClient base exception

    This class defines the base exception for GClient, and all exceptions thrown
    by GClient should inherit from this class.
    """


class GClientProtoAlreadyExistsException(GClientException):
    """Exceptions thrown for duplicate proto(submodule) registration

    This exception should be thrown if the proto(submodule) already exists while
    registering a submodule.
    """


class GClientProtoNotExistsException(GClientException):
    """Exceptions thrown for trying to use a non-existent proto(submodule)

    Exceptions that will be thrown when trying to use a non-existent proto
    (submodule).
    """


class GClientNotValidGClientUrlException(GClientException):
    """Exception thrown when an invalid GClientUrl format is parsed."""
