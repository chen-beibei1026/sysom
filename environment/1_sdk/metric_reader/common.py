# -*- coding: utf-8 -*- #
"""
Time                2023/5/25 13:33
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                common.py
Description:
"""


class StaticConst:
    """Static consts

    This class defines all the static constant values in the sls-redis module
    """

    # List of specialization parameters
    OPENTSDB_AGGREGATOR = "aggregator"
    OPENTSDB_DOWN_SAMPLE = "downsample"
    OPENTSDB_TOKEN = "token"
    TLS = "tls"

    _special_parameter_list = [
        # opentsdb aggregator
        OPENTSDB_AGGREGATOR,
        # opentsdb downsample
        OPENTSDB_DOWN_SAMPLE,
        # opentsdb token
        OPENTSDB_TOKEN,
        # true for https, false for http
        TLS,
    ]

    _sls_special_parameters_default_value = {
        OPENTSDB_AGGREGATOR: (str, "avg"),
        OPENTSDB_DOWN_SAMPLE: (str, "avg"),
        OPENTSDB_TOKEN: (str, ""),
        TLS: (bool, False)
    }

    @staticmethod
    def parse_special_parameter(params: dict) -> dict:
        """Parse specialization parameters

        Parse the specialization parameters and remove the specialization
        parameters from the parameter list

        Args:
            params(dict): CecUrl.params

        Returns:

        """
        res = {}
        for key in StaticConst._special_parameter_list:
            _type, default = \
                StaticConst._sls_special_parameters_default_value[key]
            res[key] = _type(params.pop(key, default))
        return res
