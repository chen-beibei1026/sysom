# -*- coding: utf-8 -*- #
"""
Time                2022/11/30 17:14
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                test_redis_consumer_async.py
Description:
"""
import asyncio
import unittest
import uuid
from cec_base.consumer import dispatch_consumer, Consumer
from cec_base.admin import dispatch_admin, Admin
from cec_base.producer import dispatch_producer, Producer
from cec_redis.redis_consumer import RedisConsumer
from cec_redis.redis_admin import RedisAdmin
from cec_redis.redis_producer import RedisProducer

URL = "redis://localhost:6379"


class Test(unittest.IsolatedAsyncioTestCase):
    """A test class to test RedisConsumer's async funcs
    """

    @classmethod
    def setUpClass(cls) -> None:
        """
        This initialization function is executed when the test program
        starts
        """
        Consumer.register('redis', RedisConsumer)
        Admin.register('redis', RedisAdmin)
        Producer.register('redis', RedisProducer)

    def setUp(self) -> None:
        """
        This initialization function is executed before the execution of each
        test case
        """
        self.admin = dispatch_admin(URL)
        self.redis_admin: RedisAdmin = self.admin
        self.producer = dispatch_producer(URL)

        # 1. Create a topic for testing
        self.topic_name = str(uuid.uuid4())
        self.assertEqual(self.admin.create_topic(self.topic_name), True)

        # 2. Produce some test data for consumption
        for i in range(10):
            self.producer.produce(self.topic_name, {
                "seq": i
            }, callback=lambda e, msg, idx=i: setattr(self, f"msg{idx}", msg))

        # 3. Check that the messages produced are normal (normal if they have
        #    an automatically assigned event_id)
        for i in range(10):
            self.assertNotEqual(getattr(self, f"msg{i}"), "")

    def tearDown(self) -> None:
        """
        After each test case is executed, this function is executed to perform
        some cleanup operations
        """
        print("tearDown")
        self.assertEqual(self.admin.del_topic(self.topic_name), True)

        self.admin.disconnect()
        self.producer.disconnect()

        for i in range(10):
            setattr(self, f"msg{i}", "")

    async def test_group_consume(self):
        """
        Test group consumption
        """

        # 1. Create a consumer group, which should succeed
        group1_id = str(uuid.uuid4())
        self.assertEqual(self.admin.create_consumer_group(group1_id), True)

        # 4. The first consumer joins the first consumption group, consumes 5
        #    events, and should succeed
        consumer1_id = Consumer.generate_consumer_id()
        consumer1 = dispatch_consumer(URL, self.topic_name,
                                      consumer_id=consumer1_id,
                                      group_id=group1_id)

        tasks = [consumer1.consume_async(batch_consume_limit=1) for _ in
                 range(10)]
        res = await asyncio.gather(*tasks)
        for i in range(10):
            self.assertEqual(sorted([item[0].value['seq'] for item in res])[i],
                             i)

        consumer1.disconnect()

        # 10. Delete consumer groups 1 and 2
        self.assertEqual(self.admin.del_consumer_group(group1_id), True)


if __name__ == '__main__':
    unittest.main()
