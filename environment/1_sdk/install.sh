#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
VIRTUALENV_HOME=$GLOBAL_VIRTUALENV_HOME
SDK_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

source_virtualenv() {
    echo "INFO: activate virtualenv..."
    source ${VIRTUALENV_HOME}/bin/activate || exit 1
}

install_sdk() {
    pushd ${SDK_DIR}
    python setup_clogger.py develop
    python setup_redis_lua.py develop
    python setup_cec_base.py develop
    python setup_cec_redis.py develop
    python setup_channel_job.py develop
    python setup_sysom_utils.py develop
    python setup_cmg_base.py develop
    python setup_cmg_redis.py develop
    python setup_gcache_base.py develop
    python setup_gcache_redis.py develop
    python setup_gclient_base.py develop
    python setup_gclient_http.py develop
    python setup_gclient_cmg.py develop
    python setup_metric_reader.py develop
    popd
}

install_app() {
    source_virtualenv
    install_sdk
}

install_app
