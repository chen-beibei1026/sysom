# -*- coding: utf-8 -*- #
"""
Time                2022/7/25 14:47
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                __init__.py.py
Description:
"""

from .redis_service_discovery import *
from .redis_service_registry import *

name = "cmg_redis"
