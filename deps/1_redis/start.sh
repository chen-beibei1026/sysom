#!/bin/bash
SERVICE_NAME=sysom-redis

is_start() {
    status=`supervisorctl status ${SERVICE_NAME} | awk '{print $2}'`
    result=`echo "RUNNING STARTING" | grep $status`
    if [[ "$result" != "" ]]
    then 
        return 1
    else
        return 0
    fi 
}

start_local_redis() {
    is_start
    if [[ $? == 0 ]];then
        supervisorctl start $SERVICE_NAME
        is_start
        if [[ $? == 0 ]]; then
            echo "${SERVICE_NAME} service start fail, please check log" 
            exit 1
        else
            echo "supervisorctl start ${SERVICE_NAME} success..."
        fi
    fi
}

start_redis() {
    ###we need redis version >= 5.0.0, check redis version###
    redis_version=`yum list all | grep "^redis.x86_64" | awk '{print $2}' | awk -F"." '{print $1}'`
    echo ${redis_version}
    if [ $redis_version -lt 5 ]
    then
        start_local_redis
    else
        systemctl start redis.service
    fi
}

start_app() {
    start_redis
}

start_app
