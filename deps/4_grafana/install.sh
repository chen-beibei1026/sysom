#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-prometheus
ARCH=`uname -m`
GRAFANA_PKG=grafana-9.2.2-1.${ARCH}.rpm
OSS_BASE_URL=https://sysom-publish.oss-cn-shenzhen.aliyuncs.com
MONITOR_OSS_URL=${OSS_BASE_URL}/monitor
GRAFANA_PLUGIN_OSS_URL=${OSS_BASE_URL}/sysom_deps/grafana_plugin
GRAFANA_DL_URL=https://dl.grafana.com/oss/release

install_grafana()
{
    echo "install grafana......"

    pushd ${DEPS_HOME}
    ls | grep ${GRAFANA_PKG} 1>/dev/null 2>/dev/null
    if [ $? -ne 0 ]
    then
        rpm -q --quiet wget || yum install -y wget
        wget ${MONITOR_OSS_URL}/${GRAFANA_PKG} || wget ${GRAFANA_DL_URL}/${GRAFANA_PKG}
        ls
    fi

    yum install -y ./${GRAFANA_PKG}

    # install plugin
    mkdir -p /var/lib/grafana/plugins
    pushd /var/lib/grafana/plugins
    if [ ! -f sysom-network-topology-panel-0.0.1.zip ]; then
        wget ${GRAFANA_PLUGIN_OSS_URL}/sysom-network-topology-panel-0.0.1.zip
    fi
    if [ ! -d sysom-network-topology-panel ]; then
        unzip sysom-network-topology-panel-0.0.1.zip
        rm -rf sysom-network-topology-panel-0.0.1.zip
    fi
    popd
    popd
}

install_app() {
    install_grafana
}

install_app
