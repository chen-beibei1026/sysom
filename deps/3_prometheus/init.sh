#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-prometheus

add_auto_discovery()
{
    pushd ${DEPS_HOME}/prometheus
    mkdir -p node

    cat << EOF >> prometheus.yml
  - job_name: 'auto_discovery'
    metrics_path: '/metrics'
    scheme: 'http'
    http_sd_configs:
      - url: 'http://localhost/api/v1/monitor/prometheus/sd_config'
        refresh_interval: 10s
  - job_name: "cec_monitor"
    metrics_path: "/api/v1/channel/cec_status/metrics"
    static_configs:
      - targets: ["localhost:7003"]
  - job_name: "cluster_health"
    metrics_path: "/metrics"
    static_configs:
      - targets: ["localhost:7020"]
EOF

   popd
}

init_conf() {
    cp ${SERVICE_NAME}.ini /etc/supervisord.d/
    ###change the install dir base on param $1###
    sed -i "s;/usr/local/sysom;${APP_HOME};g" /etc/supervisord.d/${SERVICE_NAME}.ini
}

init_app() {
    add_auto_discovery
    init_conf
    ###if supervisor service started, we need use "supervisorctl update" to start new conf####
    supervisorctl update
}

init_app

# Start
bash -x $BaseDir/start.sh