#!/bin/bash
#****************************************************************#
# ScriptName: deploy.sh
# Author: algalon
# Create Date: 2021-11-13 22:42
# Modify Date: 2021-11-16 00:02
# Function: deploy sysom
#***************************************************************#
APP_NAME="sysom"
APP_HOME=/usr/local/sysom
SERVER_LOCAL_IP=""
SERVER_PUBLIC_IP=""
SERVER_PORT=80

if [ $# -gt 1 ]; then
    APP_HOME=$1
fi
if [ $# -gt 2 ]; then
    SERVER_LOCAL_IP=$2
    SERVER_PUBLIC_IP=$2
fi
if [ $# -gt 3 ]; then
    SERVER_PUBLIC_IP=$3
fi
if [ $# -eq 4 ] ; then
    SERVER_PORT=$4
fi

export APP_NAME=${APP_NAME}
export APP_HOME=${APP_HOME}
export SCRIPT_HOME=${APP_HOME}/init_scripts
export SERVER_LOCAL_IP=${SERVER_LOCAL_IP}
export SERVER_PUBLIC_IP=${SERVER_PUBLIC_IP}
export SERVER_PORT=${SERVER_PORT}

mkdir -p ${APP_HOME}

if [ "$UID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
fi

BaseDir=$(dirname $(readlink -f "$0"))
deploy() {
    if [ -d "script" ]; then
        # release dir
        pushd "script"
    else
        # source code dir
        pushd `dirname ${BaseDir}`
    fi
    bash +x server/init.sh
    popd
}

deploy