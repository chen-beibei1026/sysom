#! /bin/bash
SERVICE_SCRIPT_DIR=$(basename $(dirname $0))
SERVICE_HOME=${MICROSERVICE_HOME}/${SERVICE_SCRIPT_DIR}
SERVICE_SCRIPT_DIR=$(basename $(dirname $0))
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-hotfix

init_conf() {
    cp ${SERVICE_NAME}.ini /etc/supervisord.d/
    ###change the install dir base on param $1###
    sed -i "s;/usr/local/sysom;${APP_HOME};g" /etc/supervisord.d/${SERVICE_NAME}.ini
    cpu_num=`cat /proc/cpuinfo | grep processor | wc -l`
    sed -i "s/threads = 3/threads = $cpu_num/g" ${SERVICE_HOME}/conf/hotfix_gunicorn.py
}

init_nfs()
{
    systemctl start rpcbind && systemctl enable rpcbind
    systemctl start nfs && systemctl enable nfs
    if [ $? -ne 0 ];then
        systemctl start nfs-server && systemctl enable nfs-server
    fi

    nfs_mask=`ip -4 route | grep "link src" | grep $SERVER_LOCAL_IP | awk '{print $1}' | head -n 1`
    file_path=${SERVER_HOME}/hotfix_builder/hotfix-nfs
    mkdir -p ${file_path}
    echo "${file_path} ${nfs_mask}(rw,async)" >> /etc/exports
    exportfs -rv
    chmod -R 777 ${file_path}
}

init_app() {
    init_conf
    init_nfs
    bash -x $BaseDir/db_migrate.sh
    ###if supervisor service started, we need use "supervisorctl update" to start new conf####
    supervisorctl update
}

init_app

# Start
bash -x $BaseDir/start.sh
