#!/bin/bash
SERVICE_SCRIPT_DIR=$(basename $(dirname $0))
SERVICE_HOME=${MICROSERVICE_HOME}/${SERVICE_SCRIPT_DIR}
VIRTUALENV_HOME=$GLOBAL_VIRTUALENV_HOME

source_virtualenv() {
    echo "INFO: activate virtualenv..."
    source ${VIRTUALENV_HOME}/bin/activate || exit 1
}

db_migrate() {
    pushd ${SERVICE_HOME}
    alembic upgrade head
    popd
}

source_virtualenv
db_migrate