#!/bin/bash
SERVICE_SCRIPT_DIR=$(basename $(dirname $0))
SERVICE_HOME=${MICROSERVICE_HOME}/${SERVICE_SCRIPT_DIR}
SERVICE_SCRIPT_HOME=${MICROSERVICE_SCRIPT_HOME}/${SERVICE_SCRIPT_DIR}
VIRTUALENV_HOME=$GLOBAL_VIRTUALENV_HOME
SERVICE_NAME=sysom-hotfix-builder

if [ "$UID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
fi

install_requirement() {
    pushd ${SERVICE_SCRIPT_HOME}
    pip install -r requirements.txt
    popd
}

install_package() {
    rpm -q --quiet make gcc patch bison flex openssl-devel elfutils elfutils-devel dwarves || yum install -y make gcc patch bison flex openssl-devel elfutils elfutils-devel dwarves || exit 1

    rpm -q --quiet docker git || yum install -y docker git || echo "Warngin : Docker is not installed in this machine!"
}

source_virtualenv() {
    echo "INFO: activate virtualenv..."
    source ${VIRTUALENV_HOME}/bin/activate || exit 1
}

install_app() {
    rpm -q --quiet gcc || yum install -y gcc
    rpm -q --quiet make || yum install -y make
    rpm -q --quiet nfs-utils || yum install -y nfs-utils
    rpm -q --quiet rpcbind || yum install -y rpcbind
    source_virtualenv
    install_requirement
    install_package
}

install_app
