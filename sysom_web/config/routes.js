export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/welcome',
    name: 'welcome',
    layout: false,
    hideInMenu: true,
    component: './Welcome',
  },
  {
    path: '/home',
    name: 'home',
    component: './home',
  },
  {
    path: '/host',
    name: 'host',
    routes: [
      {
        path: '/host',
        redirect: '/host/list',
      },
      {
        path: '/host/list',
        name: 'list',
        component: './host/List',
      },
      {
        path: '/host/cluster',
        name: 'cluster',
        component: './host/cluster',
      },
      {
        path: '/host/terminal/:ip?',
        component: './host/Terminal',
      }
    ],
  },
  {
    path: '/migrate',
    name: 'migrate',
    routes: [
      {
        path: '/migrate',
        redirect: '/migrate/implement',
      },
      {
        path: '/migrate/assess',
        name: 'assess',
        component: './migrate/assess',
      },
      {
        path: '/migrate/report/:id?',
        // name: 'report',
        hideInBreadcrumb: true,
        component: './migrate/assessReport',
      },
      {
        path: '/migrate/implement',
        name: 'implement',
        hideInBreadcrumb: true,
        component: './migrate/implement',
      },
    ]
  },
  {
    path: '/monitor',
    name: 'monitor',
    routes: [
      {
        path: '/monitor',
        redirect: '/monitor/dashboard',
      },

      // 集群监控
      {
        path: 'cluster_monitor',
        name: 'cluster_monitor',
        hideInBreadcrumb: true,
        component: './Monitor/ClusterDashboard',
      },
      {
        path: 'cluster_monitor/:host?',
        component: './Monitor/ClusterDashboard',
      },

      // 节点监控
      {
        path: 'node_monitor',
        name: 'node_monitor',
        hideInBreadcrumb: true,
        component: './Monitor/SysakBaseDashboard',
      },
      {
        path: 'node_monitor/:host?',
        component: './Monitor/SysakBaseDashboard',
      },

      // 容器监控
      {
        path: 'container_monitor',
        name: 'container_monitor',
        hideInBreadcrumb: true,
        component: './Monitor/ContainerDashboard',
      },
      {
        path: 'container_monitor/:host?',
        component: './Monitor/ContainerDashboard',
      },

      // 系统监控
      {
        path: 'dashboard',
        name: 'dashboard',
        hideInBreadcrumb: true,
        component: './Monitor/SystemDashboard',
      },
      {
        path: 'dashboard/:host?',
        component: './Monitor/SystemDashboard',
      },
      {
        path: 'migration',
        name: 'migration',
        hideInBreadcrumb: true,
        component: './Monitor/MigrationDashboard',
      },
      {
        path: 'migration/:host?',
        component: './Monitor/MigrationDashboard',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/app_observable',
    name: 'app_observable',
    access: 'canAdmin',
    routes: [
      // {
      //   path: '/app_observable',
      //   redirect: "/app_observable/net_topo",
      // },
      // {
      //   path: '/app_observable/net_topo',
      //   name: 'net_topo',
      //   component: "./app_observable/net_topo"
      // },
      // {
      //   path: '/app_observable/mysql',
      //   name: 'mysql',
      //   component: "./app_observable/mysql"
      // },
      // {
      //   path: "/app_observable/java",
      //   redirect: '/app_observable/process',
      // },
      // {
      //   path: "/app_observable/process",
      //   name: "process",
      //   component: "./app_observable/process"
      // },
      // {
      //   path: "/app_observable/nginx",
      //   name: "nginx",
      //   component: "./app_observable/nginx"
      // }
    ],
  },
  {
    path: '/vmcore',
    name: 'vmcore',
    routes: [
      {
        path: '/vmcore',
        redirect: '/vmcore/list',
      },
      {
        path: '/vmcore/list',
        name: 'list',
        component: './vmcore/List',
      },
      {
        path: '/vmcore/detail/:id?',
        component: './vmcore/Detail',
      },
      {
        path: '/vmcore/match',
        name: 'match',
        component: './vmcore/Match',
      },
      {
        path: '/vmcore/analyse',
        component: './vmcore/Analyse',
      },
      {
        path: '/vmcore/config',
        name: 'config',
        component: './vmcore/Config',
      },
    ],
  },
  {
    path: '/diagnose',
    name: 'diagnose',
    routes: [
      {
        path: '/diagnose',
        redirect: '/diagnose/ossre',
      },
      {
        path: '/diagnose/detail/:task_id?',
        layout: false,
        component: "./diagnose/detail"
      },
    ],
  },
  {
    path: '/security',
    name: 'security',
    routes: [
      {
        path: '/security',
        redirect: '/security/list',
      },
      {
        path: '/security/list',
        name: 'list',
        component: './security/List',
      },
      {
        path: '/security/homelist/:id?',
        component: './security/Homelist',
      },
      {
        path: '/security/historical',
        component: './security/Historical',
      },
      {
        path: '/security/historicalist/:id?',
        component: './security/Historicalist',
      },
      {
        path: '/security/viewdetails/:id?/:homename?',
        component: './security/Viewdetails',
      },
      {
        path: '/security/setting',
        component: './security/Setting',
      },
    ],
  },
  {
    path: '/hotfix',
    name: 'hotfix',
    routes: [
      {
        path: '/hotfix/formal_hotfix',
        name: 'formal',
        component: './hotfix/FormalHotfixList'
      },
      {
        path: '/hotfix',
        redirect: '/hotfix/make',
      },
      {
        path: '/hotfix/make',
        name: 'make',
        component: './hotfix/Make',
      },
      {
        path: '/hotfix/hotfix_log/:id?',
        component: './hotfix/HotfixLog'
      },
      {
        path: '/hotfix/version',
        name: 'version',
        routes: [
          {
            path: '/hotfix/version/config',
            name: 'config',
            component: './hotfix/Version/VersionConfig'
          },
          {
            path: '/hotfix/version/config',
            redirect: '/hotfix/Version/VersionConfig',
          },
          {
            path: '/hotfix/version/customize',
            name: 'customize',
            component: './hotfix/Version/VersionCustomize',
          },
        ]
      },
      {
        path: '/hotfix/released',
        name: 'released_hotfix',
        component: './hotfix/Released'
      }
    ]
  },
  {
    path: '/alarm',
    name: 'alarm',
    routes: [
      {
        path: '/alarm',
        redirect: '/alarm/list',
      },
      {
        path: '/alarm/list',
        name: 'alarm_list',
        component: './alarm/list',
      }
    ],
  },
  {
    path: '/journal',
    name: 'journal',
    routes: [
      {
        path: '/journal',
        redirect: '/journal/audit',
      },
      {
        path: '/journal/audit',
        name: 'audit',
        component: './journal/Audit',
      },
      {
        path: '/journal/task',
        name: 'task',
        component: './journal/Task',
      },
      {
        path: '/journal/node',
        name: 'node',
        component: './journal/Node'
      }
    ],
  },
  {
    path: '/account',
    access: 'canAdmin',
    routes: [
      {
        path: '/account/center',
        name: 'list',
        component: './account/List',
      },
    ]
  },
  {
    path: '/',
    redirect: '/welcome',
  },
  {
    component: './404',
  },
];
