import { useIntl } from 'umi';
import MultiGrafanaPannel from '../components/multiGrafanaPannel';

/**
 * MySQL应用观测
 * @returns 
 */
const AppObservableMysql = (props) => {
    const intl = useIntl();
    return (
        <MultiGrafanaPannel
            pannels={[
                {
                    "pannelId": "mysql_monitor",
                    "pannelName": intl.formatMessage({
                        id: 'pages.app_observable.monitor_dashboard',
                        defaultMessage: 'Monitor Dashboard',
                    }),
                    "pannelUrl": '/grafana/d/hOk70b34k/app-mysql',
                },
                {
                    "pannelId": "mysql_event",
                    "pannelName": intl.formatMessage({
                        id: 'pages.app_observable.abnormal_events',
                        defaultMessage: 'Abnormal Events',
                    }),
                    "pannelUrl": '/grafana/d/Ub__1x3Vz/app-mysql-events',
                }
            ]}
            {...props}
        />
    )
};

export default AppObservableMysql;