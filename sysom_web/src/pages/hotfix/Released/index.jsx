import { useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { useIntl, FormattedMessage } from 'umi';
import CreateReleasedHotFixForm from '../components/CreateReleasedHotFixForm'
import EditReleasedHotfixForm from '../components/EditReleasedHotfixForm';
import BulkImportReleased from '../components/BulkImportReleased';
import CustomDetail from '../components/CustomDetail';
import { getReleasedHotfixList, batchAddHotfixReleased } from '../service'

const Released = () => {
  const intl = useIntl();
  const hotfixReleasedListTableActionRef = useRef();

  const columns = [
    {
      title: <FormattedMessage id="pages.hotfix.hotfixid" defaultMessage="Hotfix ID" />,
      dataIndex: 'hotfix_id',
      key: 'hotfix_id',
      width: 120,
      copyable: true,
      tooltip: intl.formatMessage({
        id: 'pages.hotfix.tooltips.hotfixid',
        defaultMessage: 'This is the hotfix id',
      })
    },
    {
      title: <FormattedMessage id="pages.hotfix.released_kernel_version" defaultMessage="released_kernel_version" />,
      dataIndex: 'released_kernel_version',
      key: 'released_kernel_version',
      width: 180,
      ellipsis: true,
      copyable: true,
      valueType: 'input',
      tooltip: intl.formatMessage({
        id: 'pages.hotfix.tooltips.released_kernelverison',
        defaultMessage: 'This is the kernel version of the released hotfix',
      })
    },
    {
      title: <FormattedMessage id="pages.hotfix.serious" defaultMessage="serious" />,
      dataIndex: 'serious',
      valueType: 'select',
      editable: true,
      valueEnum: {
        0: {
          text: "可选安装"
        },
        1: {
          text: "建议安装"
        },
        2: {
          text: "需要安装"
        }
      },
      tooltip: intl.formatMessage({
        id: 'pages.hotfix.tooltips.serious_level',
        defaultMessage: 'To show you if this hotfix is strongly suggested tobe deployed or user to decided',
      }),
      render: (dom, record) => {
        const text = dom.props.valueEnum[record.serious].text
        return <CustomDetail tableText={text} title={'推荐信息'} detail={record.serious_explain} />
      }
    },
    {
      title: <FormattedMessage id="pages.hotfix.description" defaultMessage="description" />,
      dataIndex: 'description',
      valueType: 'input',
      editable: true,
      tooltip: intl.formatMessage({
        id: 'pages.hotfix.tooltips.description',
        defaultMessage: 'Descrption of the released hotfix',
      }),
      search: false,
      render: (_, record) => {
        return <CustomDetail
          tableText={record.description}
          detail={record.description}
          title={'补丁描述信息'}
          isEllipsis
        />
      }
    },
    {
      title: <FormattedMessage id="pages.hotfix.fix_system" defaultMessage="fix_system" />,
      dataIndex: 'fix_system',
      valueType: 'select',
      valueEnum: {
        0: {
          text: "调度"
        },
        1: {
          text: "内存"
        },
        2: {
          text: "网络"
        },
        3: {
          text: "存储"
        },
        4: {
          text: "其他"
        }
      },
      editable: true,
      tooltip: intl.formatMessage({
        id: 'pages.hotfix.tooltips.fix_system',
        defaultMessage: 'What sub system this hotfix effects',
      })
    },
    {
      title: <FormattedMessage id="pages.hotfix.released_time" defaultMessage="released_time" />,
      dataIndex: 'released_time',
      valueType: 'dateTime',
      hideInSearch: true,
      tooltip: intl.formatMessage({
        id: 'pages.hotfix.tooltips.released_time',
        defaultMessage: 'The released time of this hotfix',
      })
    },
    {
      title: <FormattedMessage id="pages.hotfix.released_time" defaultMessage="released_time" />,
      dataIndex: 'released_time',
      valueType: 'dateTimeRange',
      search: {
        transform: (value) => ({
          released_start_time: value[0],
          released_end_time: value[1],
        })
      },
      hideInTable: true,
      tooltip: intl.formatMessage({
        id: 'pages.hotfix.tooltips.released_time',
        defaultMessage: 'The released time of this hotfix',
      })
    },
    {
      title: <FormattedMessage id="pages.hotfix.tooltips.downloadlink" defaultMessage="download_link" />,
      dataIndex: 'download_link',
      valueType: 'input',
      copyable: true,
      tooltip: intl.formatMessage({
        id: 'pages.hotfix.tooltips.downloadlink',
        defaultMessage: 'The download link of this released hotfix',
      }),
      search: false
    },
    {
      title: <FormattedMessage id="pages.hotfix.deprecated" defaultMessage="deprecated" />,
      dataIndex: 'deprecated',
      key: 'deprecated',
      width: 120,
      editable: true,
      valueType: 'switch',
      search: false,
      render: (_, record) => {
        if (record.deprecated) {
          var returnval = intl.formatMessage({ id: 'pages.hotfix.yes', defaultMessage: 'Yes' })
          return returnval
        } else {
          var returnval = intl.formatMessage({ id: 'pages.hotfix.no', defaultMessage: 'No' })
          return returnval
        }
      }
    },
    {
      title: <FormattedMessage id="pages.hotfix.deprecated_info" defaultMessage="deprecated_info" />,
      dataIndex: 'deprecated_info',
      key: 'deprecated_info',
      width: 120,
      editable: (_, record) => { record.deprecated ? false : true },
      valueType: 'input',
      search: false
    },
    {
      title: <FormattedMessage id="pages.hotfix.modified_time" defaultMessage="modified_time" />,
      dataIndex: 'modified_time',
      key: 'modified_time',
      width: 120,
      editable: false,
      valueType: 'input',
      search: false
    },
    {
      title: <FormattedMessage id="pages.hotfix.modified_user" defaultMessage="modified_user" />,
      dataIndex: 'modified_user',
      key: 'modified_user',
      width: 120,
      editable: false,
      valueType: 'input',
      search: false,
    },
    {
      title: <FormattedMessage id="pages.hotfix.operation" defaultMessage="Operating" />,
      key: 'option',
      dataIndex: 'option',
      valueType: 'option',
      width: 120,
      render: (_, record) => {
        return (
          <EditReleasedHotfixForm
            record={record}
            editHotfixReleasedSuccess={() => { hotfixReleasedListTableActionRef.current?.reload() }}
          />
        )
      },
    },
  ];

  return (
    <PageContainer>
      <ProTable
        actionRef={hotfixReleasedListTableActionRef}
        toolBarRender={() => [
          <CreateReleasedHotFixForm
            addHotfixReleasedSuccess={() => { hotfixReleasedListTableActionRef.current.reload() }}
          />,
          <BulkImportReleased
            templateUrl="/resource/hotfix-released导入模板.xls"
            successCallBack={() => { hotfixReleasedListTableActionRef.current.reload() }}
            uploadHandler={batchAddHotfixReleased}
          />
        ]}
        pagination={{ showSizeChanger: true, pageSizeOptions: [10, 20], defaultPageSize: 10 }}
        columns={columns}
        request={getReleasedHotfixList}
        search={{ layout: 'vertical' }}
      />
    </PageContainer>
  )
}


export default Released;
