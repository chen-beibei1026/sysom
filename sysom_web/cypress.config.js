const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    baseUrl: "http://localhost:8000",
    // baseUrl: "http://sysom_dev.qjm253.cn",
    experimentalStudio: true,
    chromeWebSecurity: false,
    env: {
      SYSOM_ACCOUNT_USERNAME: "admin",
      SYSOM_ACCOUNT_PASSWORD: "123456"
    },
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
