/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- retran", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke retran diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/net/retran",

            // 诊断参数
            {
                "instance": "127.0.0.1",
            },

            
            // "result": {
            //     "retranList": {
            //         "data": [
            //             {
            //                 "id": "1",
            //                 "source": "192.168.0.121:22",
            //                 "destination": "106.11.31.211:51052",
            //                 "tcpState": "Established",
            //                 "caState": "open",
            //                 "type": "TLP"
            //             },
            //             {
            //                 "id": "2",
            //                 "source": "127.0.0.1:6379",
            //                 "destination": "127.0.0.1:56778",
            //                 "tcpState": "FinWait1",
            //                 "caState": "open",
            //                 "type": "TLP"
            //             }
            //         ]
            //     },
            //     "retranSummary": {
            //         "data": [
            //             {
            //                 "key": "SynRetran",
            //                 "value": 0
            //             },
            //             {
            //                 "key": "SlowStartRetran",
            //                 "value": 0
            //             },
            //             {
            //                 "key": "RtoRetran",
            //                 "value": 0
            //             },
            //             {
            //                 "key": "FastRetran",
            //                 "value": 0
            //             },
            //             {
            //                 "key": "TLP",
            //                 "value": 2
            //             },
            //             {
            //                 "key": "FailedRetran",
            //                 "value": 0
            //             },
            //             {
            //                 "key": "Other",
            //                 "value": 0
            //             }
            //         ]
            //     },
            //     "sourcePortDistribution": {
            //         "data": [
            //             {
            //                 "key": "port:22",
            //                 "value": 1
            //             },
            //             {
            //                 "key": "port:6379",
            //                 "value": 1
            //             }
            //         ]
            //     },
            //     "destPortDistribution": {
            //         "data": [
            //             {
            //                 "key": "port:51052",
            //                 "value": 1
            //             },
            //             {
            //                 "key": "port:56778",
            //                 "value": 1
            //             }
            //         ]
            //     },
            //     "sourceIpDistribution": {
            //         "data": [
            //             {
            //                 "key": "ip:192.168.0.121",
            //                 "value": 1
            //             },
            //             {
            //                 "key": "ip:127.0.0.1",
            //                 "value": 1
            //             }
            //         ]
            //     },
            //     "destIpDistribution": {
            //         "data": [
            //             {
            //                 "key": "ip:106.11.31.211",
            //                 "value": 1
            //             },
            //             {
            //                 "key": "ip:127.0.0.1",
            //                 "value": 1
            //             }
            //         ]
            //     },
            //     "summary": "diagnones result: total capture 2 retransmits"
            // },

            (result) => {
                // cy.get('.ant-statistic-content-value').click()
                // cy.get(':nth-child(1) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "发送端报文路径");
                /* ==== Generated with Cypress Studio ==== */
                cy.diagnosisTaskResultHandler(result, () => {
                    cy.get('.ant-pro-card-border.ant-pro-card-contain-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "retran overview");
                    cy.get(':nth-child(2) > [style="margin-right: -8px; margin-left: -8px;"] > :nth-child(1) > .ant-pro-card-border > [style="padding: 0px;"] > .ant-pro-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "sourcePortDistribution");
                    cy.get(':nth-child(2) > [style="margin-right: -8px; margin-left: -8px;"] > :nth-child(2) > .ant-pro-card-border > [style="padding: 0px;"] > .ant-pro-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "destPortDistribution");
                    cy.get(':nth-child(3) > [style="margin-right: -8px; margin-left: -8px;"] > :nth-child(1) > .ant-pro-card-border > [style="padding: 0px;"] > .ant-pro-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "sourceIpDistribution");
                    cy.get(':nth-child(3) > [style="margin-right: -8px; margin-left: -8px;"] > :nth-child(2) > .ant-pro-card-border > [style="padding: 0px;"] > .ant-pro-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "destIpDistribution");
                    cy.get(':nth-child(4) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "重传详情列表");
                })
                /* ==== End Cypress Studio ==== */
            })
    })
})
