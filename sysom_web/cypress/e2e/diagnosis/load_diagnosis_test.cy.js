/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- loadtask", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke loadtask diagnosis, and check result", () => {
        //cy.wait(5000)
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/cpu/loadtask",

            // 诊断参数
            {
                "instance": "127.0.0.1"
            },

            // 诊断结果处理（在此处判断诊断的结果数据是否符合预期）
            /*{
                "result": {
                    "loadavg": "1.13", 
                    "sys": "false", 
                    "irq": "false", 
                    "softirq": "false", 
                    "io": "false"
                }, 
                "task_count": {
                    "runnig tasks": "2", 
                    "uninterrupt tasks": "1"
                }, 
                "uninterrupt load": [
                    {
                        "task": "load_calc", 
                        "stack": " load_calc_func+0x57/0x130\nkthread+0xf5/0x130\nret_from_fork+0x1f/0x30\n", 
                        "weight": 1
                    }
                ], 
                "running load": [
                    {
                        "task": "argusagent", 
                        "weight": 1
                    }, 
                    {
                        "task": "aliyundun", 
                        "weight": 1
                    }
                ], 
                "flamegraph":...
            }*/
            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                // cy.get('.ant-statistic-content-value').click()
                cy.get('.ant-pro-card-border.ant-pro-card-contain-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "事件总览");
                cy.get('.ant-statistic-content-value-decimal').should(($element) => {
                    let lines = $element.text().split("\n")
                    let first_line = lines[0]
                    expect(parseFloat(first_line)).to.be.gt(-1)
                });
                cy.get(':nth-child(2) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should("match", /正常|异常/);
                cy.get(':nth-child(3) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should("match", /正常|异常/);
                cy.get(':nth-child(4) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should("match", /正常|异常/);
                cy.get(':nth-child(5) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should("match", /正常|异常/);

                cy.get(':nth-child(1) > .ant-pro-card-border > [style="padding: 0px;"] > .ant-pro-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "R/D状态进程数量");
                cy.get(':nth-child(2) > .ant-pro-card-border > [style="padding: 0px;"] > .ant-pro-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "D状态负载影响度");
                cy.get(':nth-child(3) > .ant-pro-card-border > [style="padding: 0px;"] > .ant-pro-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "R状态负载影响度");
                cy.get('[style="margin-top: 16px;"] > .ant-pro-card-header').should("contain.text", "调度火焰图");;
                //cy.get('.ant-statistic-content-value').should("contain.text", "total")
                /* ==== End Cypress Studio ==== */
            })
    })
})