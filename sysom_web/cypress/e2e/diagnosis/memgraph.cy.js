/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- memgraph", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke oomcheck diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/memory/memgraph",

            // 诊断参数
            {
                "instance": "127.0.0.1"
            },

            // 诊断结果处理（在此处判断诊断的结果数据是否符合预期）
            /*{
               "id": 1625,
                "created_at": "2023-09-21 15:57:19",
                "updated_at": "2023-09-21 15:57:19",
                "task_id": "TMjO3sh0",
                "status": "Success",
                "service_name": "memgraph",
                "code": 0,
                "err_msg": "",
                "result": {
                    "summary": "success",
                    "dataMemEvent": {
                        "data": [
                            {
                                "key": "Util",
                                "value": 19.0
                            },
                            {
                                "key": "MemLeak",
                                "value": "OK"
                            },
                            {
                                "key": "MemcgLeak",
                                "value": "OK"
                            },
                            {
                                "key": "MemFrag",
                                "value": "OK"
                            }
                        ]
                    },
                    "dataMemOverView": {
                        "data": [
                            {
                                "key": "app",
                                "value": 11975288
                            },
                            {
                                "key": "free",
                                "value": 1354536
                            },
                            {
                                "key": "kernel",
                                "value": 2941968
                            }
                        ]
                    },
                    "dataKerMem": {
                        "data": [
                            {
                                "key": "SReclaimable",
                                "value": 1891644
                            },
                            {
                                "key": "VmallocUsed",
                                "value": 30576
                            },
                            {
                                "key": "allocPage",
                                "value": 157600
                            },
                            {
                                "key": "KernelStack",
                                "value": 9216
                            },
                            {
                                "key": "PageTables",
                                "value": 37308
                            },
                            {
                                "key": "SUnreclaim",
                                "value": 125552
                            },
                            {
                                "key": "reserved",
                                "value": 690072
                            }
                        ]
                    },
                    "dataUserMem": {
                        "data": [
                            {
                                "key": "filecache",
                                "value": 9218024
                            },
                            {
                                "key": "anon",
                                "value": 2289968
                            },
                            {
                                "key": "mlock",
                                "value": 0
                            },
                            {
                                "key": "huge1G",
                                "value": 0
                            },
                            {
                                "key": "huge2M",
                                "value": 0
                            },
                            {
                                "key": "buffers",
                                "value": 467132
                            },
                            {
                                "key": "shmem",
                                "value": 2208
                            }
                        ]
                    },
                    "dataCacheList": {
                        "data": [
                            {
                                "key": 0,
                                "Name": "/var/lib/mysql/sysom/sys_handler_log.ibd",
                                "cached": 300180,
                                "Task": "mysqld_78575 "
                            },
                            {
                                "key": 1,
                                "Name": "/var/log/sysom/sysom-migration-access.log",
                                "cached": 146116,
                                "Task": "gunicorn_33647 ,gunicorn_33685 ,gunicorn_380706 ,gunicorn_469901 ,gunicorn_1365696 "
                            },
                            {
                                "key": 2,
                                "Name": "/var/log/sysom/sysom-rtdemo-access.log",
                                "cached": 119404,
                                "Task": "gunicorn_60718 ,gunicorn_445691 ,gunicorn_474549 "
                            },
                            {
                                "key": 3,
                                "Name": "/var/log/sysom/sysom-monitor-server-access.log",
                                "cached": 104372,
                                "Task": "gunicorn_33682 ,gunicorn_454861 ,gunicorn_479093 "
                            },
                            {
                                "key": 4,
                                "Name": "/var/log/sysom/sysom-channel-access.log",
                                "cached": 95352,
                                "Task": "gunicorn_33233 ,gunicorn_315037 ,gunicorn_455831 "
                            },
                            {
                                "key": 5,
                                "Name": "total cached of close file",
                                "cached": 5980068,
                                "Task": ""
                            }
                        ]
                    },
                    "dataProcMemList": {
                        "data": [
                            {
                                "key": 0,
                                "task": "mysqld",
                                "MemTotal": 149156,
                                "RssAnon": 126284,
                                "RssFile": 22872
                            },
                            {
                                "key": 1,
                                "task": "gunicorn",
                                "MemTotal": 144156,
                                "RssAnon": 113764,
                                "RssFile": 30392
                            },
                            {
                                "key": 2,
                                "task": "gunicorn",
                                "MemTotal": 143036,
                                "RssAnon": 112700,
                                "RssFile": 30336
                            },
                            {
                                "key": 3,
                                "task": "grafana-server",
                                "MemTotal": 103508,
                                "RssAnon": 42568,
                                "RssFile": 60940
                            },
                            {
                                "key": 4,
                                "task": "gunicorn",
                                "MemTotal": 97412,
                                "RssAnon": 76224,
                                "RssFile": 21188
                            },
                            {
                                "key": 5,
                                "task": "gunicorn",
                                "MemTotal": 97332,
                                "RssAnon": 76276,
                                "RssFile": 21056
                            },
                            {
                                "key": 6,
                                "task": "prometheus",
                                "MemTotal": 95300,
                                "RssAnon": 45304,
                                "RssFile": 49996
                            },
                            {
                                "key": 7,
                                "task": "systemd-journal",
                                "MemTotal": 89644,
                                "RssAnon": 45000,
                                "RssFile": 44640
                            },
                            {
                                "key": 8,
                                "task": "gunicorn",
                                "MemTotal": 89644,
                                "RssAnon": 76012,
                                "RssFile": 13632
                            },
                            {
                                "key": 9,
                                "task": "gunicorn",
                                "MemTotal": 89456,
                                "RssAnon": 75796,
                                "RssFile": 13660
                            }
                        ]
                    }
                },
                "params": {
                    "service_name": "memgraph",
                    "channel": "ssh",
                    "instance": "192.168.0.130"
                },
                "created_by": 1,
            }*/
            (result) => {
                // result => 包含诊断API返回的诊断详情数据
                cy.get('table > tbody').eq(1).children().should('have.length',10)
                cy.get('table > tbody').eq(2).children().should('have.length',6)

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                // cy.get(':nth-child(1) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should('match',/^[0-9]*%$/);
                cy.get(':nth-child(1) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').contains(/\d+/).then(($el) => {
                    const num = parseInt($el.text());
                    expect(num).to.be.gt(0);
                })
                cy.get(':nth-child(2) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should('match',/NG|OK/);
                cy.get(':nth-child(3) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should('match',/NG|OK/);
                cy.get(':nth-child(4) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should('match',/NG|OK/);
                /* ==== End Cypress Studio ==== */
            })
    })
})
