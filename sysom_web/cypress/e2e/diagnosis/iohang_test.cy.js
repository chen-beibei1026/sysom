/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- iohang", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke iohang diagnosis, and check result", () => {
        //cy.wait(5000)
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/storage/iohang",

            // 诊断参数
            {
                "instance": "127.0.0.1",
                "threshold": "1",
                "timeout": "10",
            },
            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                // cy.get('.ant-statistic-content-value').click()
                cy.diagnosisTaskResultHandler(result, () => {
                    cy.get('.ant-pro-card-border.ant-pro-card-contain-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "IO HANG overview");
                    cy.get(':nth-child(1) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should("match", /normal|abnormal/);
                    cy.get(':nth-child(2) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "More details of TOP 10 IO");
                })
                //cy.get('.ant-statistic-content-value').should("contain.text", "total")
                /* ==== End Cypress Studio ==== */
            })
    })
})