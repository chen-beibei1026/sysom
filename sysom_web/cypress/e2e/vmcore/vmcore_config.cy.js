/// <reference types="cypress" />

describe("vmcore list page", () => {
    it("show vmcore list", () => {
        cy.intercept("POST","/api/v1/vmcore/vmcore_config_test").as("vmcoreTest")
        cy.login()
        cy.visit("/vmcore/config")
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#name').clear();
        cy.get('#name').type("testconfig");
        cy.get('#server_host').clear();
        cy.get('#server_host').type("127.0.0.1");
        cy.get('#mount_point').clear();
        cy.get('#mount_point').type("/tmp/vmcore-nfs");
        cy.get('#days').clear();
        /* ==== Generated with Cypress Studio ==== */
        cy.get('.ml20___wnEIC').click();
        cy.get(':nth-child(1) > .ant-descriptions-item > .ant-descriptions-item-container > .ant-descriptions-item-content').should('have.text',"testconfig");
        cy.get(':nth-child(2) > .ant-descriptions-item > .ant-descriptions-item-container > .ant-descriptions-item-content').should('have.text',"127.0.0.1");
        cy.get(':nth-child(3) > .ant-descriptions-item > .ant-descriptions-item-container > .ant-descriptions-item-content').should('have.text',"/tmp/vmcore-nfs");
        //cy.get('.configleft17___1xVO3').click();
        /* ==== End Cypress Studio ==== */
    })
})
