/// <reference types="cypress" />

describe("vmcore list page", () => {
    it("show vmcore list", () => {
        cy.login()
        cy.visit("/vmcore/list")
        /* ==== Generated with Cypress Studio ==== */
        cy.get(':nth-child(1) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value > .ant-statistic-content-value-int').invoke('text').should('match',/^\d+/);
        cy.get(':nth-child(3) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value > .ant-statistic-content-value-int').invoke('text').should('match',/^\d+/);
        cy.get(':nth-child(5) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value ').invoke('text').should('match',/^\d+\.\d+$/);
        cy.get(':nth-child(7) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value ').invoke('text').should('match',/^\d+\.\d+$/);
        /* ==== End Cypress Studio ==== */
    })
})
