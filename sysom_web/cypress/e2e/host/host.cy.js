/// <reference types="cypress" />

describe("SysOM Host Manager Test", () => {
    beforeEach(() => {
        cy.login()
    })
    it("Crate and delete host", () => {
        cy.intercept("POST", "/api/v1/host/")
            .as("createHost")

        cy.intercept("DELETE", "/api/v1/host/*")
            .as("deleteHost")

        // 1. 访问主机列表也米娜
        cy.visit("/host/list")

        // 2. 点击新建主机打开模块框
        cy.get("button").contains("新建主机").click()

        // 3. 在模态框内部填充字段
        cy.get(".ant-modal-content").first().within(() => {
            // 3.1 cluster
            cy.get("#cluster").focus().type("default").type("{enter}")

            // 3.2 hostname
            cy.get("#hostname").focus().clear().type("local") 

            // 3.3 username
            cy.get("#username").focus().clear().type("root")

            // 3.4 password
            cy.get("#host_password").focus().clear().type("alios#123")

            // 3.5 ip
            cy.get("#ip").focus().clear().type("127.0.0.1")

            // 3.6 port
            cy.get("#port").focus().clear().type("22")

            // 3.7 确认
            cy.get("button").contains("确 认").click()

            // 3.8 等待新建主机请求结束，判断请求是否成功
            // 检查状态码返回是否是200（如果集群已经存在会返回400）
            cy.wait('@createHost').its("response.statusCode").should("eq", 200)
        })

        // 创建完主机后等待一秒钟，一秒钟后执行删除操作
        cy.wait(1000)

        // 找到最新创建的主机，并点击删除按钮
        cy.get("td")
            .contains("127.0.0.1")
            .parent()
            .within(() => {
                cy.get("td").contains("删除").click()
            })

        // 点击删除按钮之后需要在弹出的浮窗中点击OK确认
        cy.get(".ant-popover-buttons").find("button").contains("OK").click()

        // 确认删除接口调用结果为 200
        cy.wait('@deleteHost')
            .then((interception) => {
                cy.wrap({
                    statusCode: interception.response?.statusCode
                }).its("statusCode").should("eq", 200)
            })
    })
})